# pivot_table

## Usage

```bash
# il ya un moment faut faire un .env avec USER,PASSWORD,HOST,DATABASE 
git clone git@gitlab.com:tech-data3/pivot_table.git
cd pivot_table
sudo mysql < scripts/init.sql
# la il faut mettre des droits a quelqu un (le meme que dans le .env) Grant ... on ... to
python3 -m venv ./.venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -e .
# la il faut vraiment avoir le .env !
python src/pivot_table/main.py
deactivate
```

pour l'instant ca extrait les donnees du xlsx et ca les ecrit dans la bdd.
il faut faire un writer json pour ressortir les donnees (et pour les pivot table il faudrait une classe mutateur)

## consignes

je sais plus je dois faire des objets je crois.

## EDA

### first analyse

Les noms ont l'air correctement normalise

En effet lorsque l'on compte les lignes contenant des champs textes identique, il n'y a pas 
different orthographe pour le meme mot.

On se rend compte que les country, les categorie et les produit sont des relations many to many

aucun Order ID ne semble etre repeter
## modelisation

### first through

#### MCD

```plantuml
@startuml
skinparam componentStyle rectangle
[order] --> [product]: concerne
[product] --> [category]: font partie d'une
[order] --> [amount]: coute un certain
[order] --> [pays]: concerne un client dans un

@enduml
```

#### MLD
```plantuml
@startuml
json order {
   "order_id":"INT, PK",
   "date":"datetime",
   "amount": "int"
}

json attribute {
   "attribute_type":["country", "product", "category"],
   "value":"varchar",
   "parent_id":"int pk"
}
order "0..*" - "1..*" attribute
@enduml
```