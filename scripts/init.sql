DROP DATABASE IF EXISTS pivot_table;
CREATE DATABASE pivot_table;

USE pivot_table;

CREATE TABLE `order`(
    order_id INT PRIMARY KEY AUTO_INCREMENT,
    amount INT,
    date DATE
);

create TABLE attribute(
    attribute_id INT AUTO_INCREMENT PRIMARY KEY,
    type ENUM('product', 'category', 'country') NOT NULL,
    value VARCHAR(100) NOT NULL,
    parent_id INT REFERENCES attribute.attribute_id
);

CREATE TABLE order_attribute(
    attribute_id INT REFERENCES attribute.attribute_id,
    order_id INT REFERENCES `order`.order_id,
    PRIMARY KEY(attribute_id, order_id)
);