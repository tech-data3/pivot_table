USE pivot_table;
-- level 1
-- pivot_table 1
SELECT subr2.country, subr2.product, count(*) AS `nb vente` FROM
    (SELECT subr.order_id, subr.amount, subr.date, max(subr.product) as product, max(subr.country) as country, max(subr.category) as category FROM
        (SELECT
            o.*,
            case when a.type = "product" then a.value end as `product`,
            case when a.type = "country" then a.value end as `country`,
            case when a2.type then a2.value end as `category`
        FROM
            `order` AS o
        JOIN
            order_attribute AS oa ON oa.order_id = o.order_id
        JOIN
            attribute AS a ON a.attribute_id = oa.attribute_id
        LEFT JOIN
            attribute AS a2 ON a2.attribute_id = a.parent_id
        ) as subr
    GROUP BY
        subr.order_id
    ) AS subr2
    where subr2.country in ("United States", "United Kingdom")
    group by
        subr2.country, subr2.product
    order by
        subr2.country, `nb vente` DESC
;

-- level2
-- pivot_table2 OUCH wait for it