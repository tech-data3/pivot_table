import os

from dotenv import load_dotenv
from sqlalchemy import create_engine

load_dotenv()


class MySQLEngine:
    instance = None

    @staticmethod
    def get_engine():
        if not MySQLEngine.instance:
            MySQLEngine.instance = create_engine(
                f"mysql+pymysql://{os.environ["USER"]}:{os.environ["PASSWORD"]}@{os.environ["HOST"]}/{os.environ["DATABASE"]}",
                echo=True)
        return MySQLEngine.instance
