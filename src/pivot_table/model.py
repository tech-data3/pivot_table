from __future__ import annotations

import datetime
from typing import List

from sqlalchemy import String, ForeignKey, Column, Table
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship


class Serializable:
    def serialize(self):
        result = {}
        for key in self.__table__.columns.keys():
            value = getattr(self, key)
            if isinstance(value, list):
                value = [v.serialize() if isinstance(v, Serializable) else v for v in value]
            elif isinstance(value, Serializable):
                value = value.serialize()
            result[key] = value
        return result


class Base(DeclarativeBase):
    pass


# note for a Core table, we use the sqlalchemy.Column construct,
# not sqlalchemy.orm.mapped_column
association_table = Table(
    "order_attribute",
    Base.metadata,
    Column("attribute_id", ForeignKey("attribute.attribute_id"), primary_key=True),
    Column("order_id", ForeignKey("order.order_id"), primary_key=True),
)


class Order(Base, Serializable):
    __tablename__ = 'order'

    order_id: Mapped[int] = mapped_column(primary_key=True)
    amount: Mapped[int] = mapped_column()
    date: Mapped[datetime.date] = mapped_column()
    attributes: Mapped[List[Attribute]] = relationship(
        secondary=association_table
    )

    def __repr__(self) -> str:
        return f"order(order_id: {self.order_id}, amount: {self.amount}, date: {self.date}, attributes: {self.attributes})"


class Attribute(Base, Serializable):
    __tablename__ = 'attribute'

    attribute_id: Mapped[int] = mapped_column(primary_key=True)
    type: Mapped[str] = mapped_column(String(50))
    value: Mapped[str] = mapped_column(String(100))
    parent_id: Mapped[int] = mapped_column(ForeignKey("attribute.attribute_id"))
    parent: Mapped[Attribute] = relationship(remote_side=[attribute_id])

    def __repr__(self) -> str:
        return f"Attribute({self.attribute_id}, {self.type}, {self.value}, {self.parent_id}, {self.parent})"
