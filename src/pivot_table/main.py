from pivot_table import extractor, writer


def main():
    e = extractor.XLExtractor("data/pivot-tables.xlsx")
    w = writer.MySQLWriter()
    w.write(e.extract())


if __name__ == "__main__":
    main()
