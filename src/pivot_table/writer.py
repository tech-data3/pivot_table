from sqlalchemy.orm import Session

from pivot_table import dependencies
from pivot_table.model import Order, Attribute


class MySQLWriter:
    engine = None

    def __init__(self):
        if not MySQLWriter.engine:
            MySQLWriter.engine = dependencies.MySQLEngine.get_engine()

    def write(self, dict_list):
        obj = []

        def obj_in_list(object_list, **kwargs):
            """
            retourne une instance d'un objet deja croisee
            :param object_list:
            :param kwargs:
            :return:
            """
            for obj in object_list:
                for k, v in kwargs.items():
                    if not (hasattr(obj, k) and getattr(obj, k) == v):
                        break
                else:
                    return obj
            return None

        orders = []
        with Session(MySQLWriter.engine) as session:
            for line in dict_list:
                order = Order(amount=line.get("amount"), date=line.get("date"))
                for key in ["product", "country"]:
                    key_ = {"type": key, "value": line.get(key)}
                    if not (cached_obj := obj_in_list(obj, **key_)):
                        cached_obj = Attribute(**key_)
                        if key == "product":
                            cat = {"type": "category", "value": line.get("category")}
                            parent = obj_in_list(obj, **cat)
                            if not parent:
                                obj.append(Attribute(**cat))
                                parent = obj[-1]
                            cached_obj.parent = parent
                            print(cached_obj.parent)
                    obj.append(cached_obj)
                    order.attributes.append(cached_obj)
                orders.append(order)
            session.add_all(orders)
            session.commit()
