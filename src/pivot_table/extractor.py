import pathlib

import pandas as pd
from sqlalchemy import select
from sqlalchemy.orm import joinedload, Session

from pivot_table import dependencies
from pivot_table.model import Order, Attribute


class XLExtractor:
    def __init__(self, path: str):
        self.source = pathlib.Path.cwd().joinpath(path)
        print(self.source)
        if not self.source.is_file():
            raise ValueError(f"{path} not a file")

    def extract(self):
        self.df = pd.read_excel(self.source, sheet_name="Sheet1")
        # ca ressemble a une standardisation des nom de colone ca !
        self.df.rename(columns={k: k.lower().strip().replace(" ", "_") for k in self.df.columns}, inplace=True)
        return self.df.to_dict("records")


class MySQLExtractor:
    engine = None

    def __init__(self):
        self.engine = dependencies.get_engine()

    def serialize_order(obj_list):
        results = []
        for obj in obj_list:
            result = {}
            for attr in obj.attributes:
                if attr.parent:
                    result["category"] = attr.parent.value
                result[attr.type] = attr.value
            for key in ["order_id", "amount", "date"]:
                result.update({key: getattr(obj, key)})
            results.append(result)
        return results

    def execute_select_line_by_countries(self, countries: list[str]) -> pd.DataFrame:
        with (Session(MySQLExtractor.engine) as session):
            result = session.scalars(
                select(Order).options(joinedload(Order.attributes)).where(
                    Order.attributes.any(Attribute.value.in_(countries)))
            ).unique().all()
            return pd.DataFrame(self.serialize_order(result))

    def get_pivot_table_1(df: pd.DataFrame):
        return df[['country', 'product']].groupby(['country', 'product']).value_counts()

    def get_pivot_table_2(df: pd.DataFrame):
        return df[['country', 'product', 'amount']].groupby(['country', 'product']).sum().unstack(1)
